rule Yandex
{
    meta:
        author        = "Brie Carranza"
        description   = "Yara rule containing right hand sides for email addresses from Yandex "

    strings:
        $yandex00 = "@yandex.by"
        $yandex01 = "@yandex.com"
        $yandex02 = "@yandex.ru"

    condition:
        any of them
}
