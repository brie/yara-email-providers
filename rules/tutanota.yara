rule Tutanota
{
    meta:
        author        = "Brie Carranza"
        description   = "Yara rule containing right hand sides for email addresses from Tutanota // https://tutanota.com/faq/ "

    strings:
        $tutanota_com = "@tutanota.com"
        $tuta_io = "@tuta.io"
        $tuta_mail = "@tutamail.com"
        $dot_de = "@tutanota.de"
        $keemail_me = "@keemail.me"

    condition:
        any of them
}
