rule Mailru
{
    meta:
        author        = "Brie Carranza"
        description   = "Yara rule containing right hand sides for email addresses from Mail.ru "

    strings:
        $mail_ru = "@mail.ru"
        $inbox_ru = "@inbox.ru"
        $list_ru = "@list.ru"
        $bk_ru = "@bk.ru"

    condition:
        any of them
}
