rule Msgsafe
{
    meta:
        author        = "Brie Carranza"
        description   = "Yara rule containing right hand sides for email addresses from msgsafe.io"

    strings:
        $msgsafe_io00 = "@msgsafe.io"
        $msgsafe_io01 = "@nerdmail.co"
        $msgsafe_io02 = "@confidential.tips"
        $msgsafe_io03 = "@yahooweb.co"
        $msgsafe_io04 = "@messageden.net"
        $msgsafe_io05 = "@messagesafe.io"
        $msgsafe_io06 = "@offtherecordmail.com"
        $msgsafe_io07 = "@msgden.com"
        $msgsafe_io08 = "@inboxhub.net"
        $msgsafe_io09 = "@decoymail.net"
        $msgsafe_io10 = "@stealthypost.org"
        $msgsafe_io11 = "@decoymail.com"
        $msgsafe_io12 = "@messageden.com"
        $msgsafe_io13 = "@msgsafe.ninja"
        $msgsafe_io14 = "@privyinternet.net"
        $msgsafe_io15 = "@speakfreely.legal"
        $msgsafe_io16 = "@techmail.info"
        $msgsafe_io17 = "@bingzone.net"
        $msgsafe_io18 = "@messagesafe.co"
        $msgsafe_io19 = "@privyinternet.com"
        $msgsafe_io20 = "@outlookpro.net"
        $msgsafe_io21 = "@armormail.net"
        $msgsafe_io22 = "@messagesafe.ninja"
        $msgsafe_io23 = "@happygoluckyclub.com"
        $msgsafe_io24 = "@xyzmailhub.com"
        $msgsafe_io25 = "@showme.social"
        $msgsafe_io26 = "@decoymail.mx"
        $msgsafe_io27 = "@webmeetme.com"
        $msgsafe_io28 = "@concealed.company"
        $msgsafe_io29 = "@muttwalker.net"
        $msgsafe_io30 = "@snugmail.net"
        $msgsafe_io31 = "@reddithub.com"
        $msgsafe_io32 = "@airmailhub.com"
        $msgsafe_io33 = "@meet-me.live"
        $msgsafe_io34 = "@speakfreely.email"
        $msgsafe_io35 = "@smime.ninja"
        $msgsafe_io36 = "@privyonline.net"
        $msgsafe_io37 = "@msghideaway.net"
        $msgsafe_io38 = "@theopposition.club"
        $msgsafe_io39 = "@msgden.net"
        $msgsafe_io40 = "@stealthypost.net"
        $msgsafe_io41 = "@privyonline.com"
        $msgsafe_io42 = "@unicorntoday.com"
        $msgsafe_io43 = "@xyzmailpro.com"
        $msgsafe_io44 = "@confidential.life"
        

    condition:
        any of them
}
