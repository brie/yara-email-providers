rule ProtonMail
{
    meta:
        author        = "Brie Carranza"
        description   = "Yara rule containing right hand sides for email addresses from ProtonMail "

    strings:
        $proton_dot_me = "@proton.me"
        $proton_mail_com = "@protonmail.com"
        $proton_mail_ch = "@protonmail.ch"
        $proton_mail_short = "@pm.me"

    condition:
        any of them
}
