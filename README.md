# ✨ README

## 🔧 Usage

The simplest syntax for running YARA at the CLI is:

```
yara [OPTIONS] RULES_FILE TARGET
```

Assuming you have [already installed YARA](https://yara.readthedocs.io/en/latest/gettingstarted.html) and `PWD` is the root of this repository, you can run:

```
yara $(find rules -iname \*yara) samples
```

The [substitution](https://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html) with `find` is to convince `yara` to 🥺 look at multiple different rules files. (It's not currently possible to pass the directory and ask YARA to please iterate through every file therein.)

The results will show which rule hit and which file has a match for the rule.


## 📚 Resources

- [Running YARA from the command-line](https://yara.readthedocs.io/en/stable/commandline.html#running-yara-from-the-command-line) 
